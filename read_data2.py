#! /usr/bin/env python
"""Import data and convert to a Healpix map."""
import numpy as np
import healpy as hp
import logging
from scipy.spatial import cKDTree
from read_data import get_rcs, declratoindex
NSIDE = 2048
SIZE = 0
maxra = 0
minra = 0
maxdec = 0
mindec = 0


def make_finaldata():
    """
    Make the finaldata array needed for make_map().

    Parameters
    ----------
    None

    Returns
    -------
    None

    Raises
    ------
    None

    See Also
    --------
    None

    Notes
    -----
    None

    """
    tree, e1, e2, w, m = get_rcs()
    finaldata = np.column_stack([tree.data, e1, e2, w, m])
    logging.info(f"Length of final data {len(finaldata)}")
    return finaldata


def make_map(finaldata):
    """
    Make healpix map from data.

    Parameters
    ----------
    finaldata: Numpy array
    The array needs to have
    coords, e1, e2, weightmap, shearcalibmap
    shearcalibmap in that order

    Returns
    -------
    Healpix Map

    Raises
    ------
    None

    See Also
    --------
    None

    Notes
    -----
    None

    """
    e1map = np.full(hp.nside2npix(NSIDE), hp.UNSEEN, dtype=np.float)
    e2map = np.full(hp.nside2npix(NSIDE), hp.UNSEEN, dtype=np.float)
    weightmap = np.full(hp.nside2npix(NSIDE), hp.UNSEEN, dtype=np.float)
    shearcalibmap = np.full(hp.nside2npix(NSIDE), hp.UNSEEN, dtype=np.float)
    existance = np.full(hp.nside2npix(NSIDE), False, dtype=np.bool)
    etangent = np.full(hp.nside2npix(NSIDE), hp.UNSEEN, dtype=np.float)
    for k in finaldata:
        index = declratoindex(k[1], k[0], NSIDE)
        if not existance[index]:
            e1map[index] = 0
            e2map[index] = 0
            weightmap[index] = 0
            shearcalibmap[index] = 0
            existance[index] = True
            etangent[index] = 0
        e1map[index] += k[2]
        e2map[index] += k[3]
        weightmap[index] += k[4]
        shearcalibmap[index] += k[5]
        etangent[index] += k[4]*(-k[2]*np.cos(np.radians(2*k[0])) 
                        - k[3]*np.sin(np.radians(2*k[0])))

    return e1map, e2map, weightmap, shearcalibmap, existance, etangent


def indextodeclra(index):
    """
    Convert index to angles.

    Parameters
    ----------
    index: Int
        Healpix pixel index

    Returns
    -------
    Decl, RA: Float
        Declination and right ascention

    Raises
    ------
    None

    See Also
    --------
    DeclRaToIndex()

    Notes
    -----
    None

    """
    decl, ra = np.degrees(hp.pix2ang(NSIDE, index))
    return [90. - decl, ra]


def make_healpix_coord_tree(existance):
    """
    Make a CKDTree for healpix pixels.

    Parameters
    ----------
    None

    Returns
    -------
    CKDTree

    Raises
    ------
    None

    See Also
    --------
    None

    Notes
    -----
    None

    """
    import tqdm
    length = hp.nside2npix(NSIDE)
    coords = []
    for i in tqdm.tqdm(range(length)):
        if existance[i]:
            coords.append(indextodeclra(i))
        else:
            coords.append([hp.UNSEEN, hp.UNSEEN])
    return np.array(coords)


def read_avg_data():
    """
    Read the averaged over skymap data from the fits file.

    Parameters
    ----------
    None

    Returns
    -------
    Data

    Raises
    ------
    None

    See Also
    --------
    None

    Notes
    -----
    None

    """
    verbose = logging.getLogger().getEffectiveLevel() < 21
    dec = hp.read_map('coordmap_dec.fits', verbose=verbose)
    ra = hp.read_map('coordmap_ra.fits', verbose=verbose)
    exist = hp.read_map('exist.fits', verbose=verbose)
    e1map = hp.read_map('e1.fits', verbose=verbose)
    e2map = hp.read_map('e2.fits', verbose=verbose)
    wmap = hp.read_map('w.fits', verbose=verbose)
    mmap = hp.read_map('m.fits', verbose=verbose)
    coord = []
    e1 = []
    e2 = []
    w = []
    m = []
    for i in range(len(exist)):
        if exist[i]:
            coord.append([ra[i], dec[i]])
            e1.append(e1map[i])
            e2.append(e2map[i])
            w.append(wmap[i])
            m.append(mmap[i])

    coord = np.array(coord)
    global maxra
    maxra = max(coord[:, 0])
    global minra
    minra = min(coord[:, 0])
    global maxdec
    maxdec = max(coord[:, 1])
    global mindec
    mindec = min(coord[:, 1])
    global bsize
    bsize = abs(max(maxra, maxdec) - min(mindec, minra))
    global SIZE
    SIZE = len(coord)
    logging.info(f"Max RA: {maxra} Max Dec: {maxdec}")
    logging.info(f"Min RA: {minra} Min Dec: {mindec}")
    logging.info(f"Size of sample loaded: {SIZE}")
    # Making the cKDTree
    ctree = cKDTree(coord)
    return ctree, np.array(e1), np.array(e2), np.array(w), np.array(m)


if __name__ == '__main__':
    print("Reading data")
    e1, e2, w, m, exist, et = make_map(make_finaldata())
    print("writing data")
    hp.write_map('e1.fits', e1, overwrite=True)
    hp.write_map('e2.fits', e2, overwrite=True)
    hp.write_map('w.fits', w, overwrite=True)
    hp.write_map('m.fits', m, overwrite=True)
    hp.write_map('exist.fits', exist, overwrite=True)
    hp.write_map('et.fits', et, overwrite=True)
    del e1
    del e2
    del w
    del m
    del et
    print("Making coord data")
    ctree = make_healpix_coord_tree(exist)
    del exist
    print("Write coords")
    hp.write_map('coordmap_ra.fits', ctree[:, 0])
    hp.write_map('coordmap_dec.fits', ctree[:, 1])
    del ctree
    # ctree, e1, e2, w, m = read_avg_data()
